import logging

from logging_prometheus.setup import PrometheusHandler


class TestPrometheusHandler(object):
    def test_init_counter(self):
        h1 = PrometheusHandler('test_1', ['test_1'])
        h2 = PrometheusHandler('test_1', ['test_1', 'test_2'])
        h3 = PrometheusHandler('test_2', ['test_1'])

        assert id(h1.counter) == id(h2.counter)
        assert id(h1.counter) != id(h3.counter)

    def test_init_counter(self):
        handler = PrometheusHandler()

        logger = logging.getLogger('app')
        logger.addHandler(handler)

        logger.debug('debug')
        logger.info('info')
        logger.warning('warning')
        logger.error('error')
