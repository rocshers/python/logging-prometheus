from logging_prometheus.handlers import PrometheusHandler  # noqa: F401
from logging_prometheus.setup import setup_prometheus_handler_for_root  # noqa: F401
