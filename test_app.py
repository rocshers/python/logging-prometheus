import logging
from pprint import pp

from prometheus_client import REGISTRY

from logging_prometheus import setup_prometheus_handler_for_root

setup_prometheus_handler_for_root()

logger = logging.getLogger('app')

logger.debug('debug')
logger.info('info')
logger.warning('warning')
logger.error('error')


for m in REGISTRY.collect():
    pp(m)
